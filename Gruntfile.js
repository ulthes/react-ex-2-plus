/*jslint node: true */
module.exports = function(grunt) {
    /* Load grunt tasks from package.json in devDependencies */
    require('jit-grunt')(grunt, {
        bower: 'grunt-bower-task'
    });
    /* Display tasks' elapsed execution time */
    require('time-grunt')(grunt);

    grunt.config.init({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            default: {
                options: {
                    paths: ['src/app/content/styles/less']
                },
                files: {
                    'dist/content/css/base.css': 'src/app/content/styles/less/base/style.less',
                    'dist/content/css/main.css': 'src/app/content/styles/less/main/style.less'
                }
            }
        },
        browserify: {
            dev: {
                options: {
                    browserifyOptions: {
                        debug: true,
                        transform: ['reactify'],
                        extensions: ['.jsx']
                    }
                },
                files: [{
                    expand: true,
                    cwd: './src/app/jsx',
                    src: ['**/*.jsx'],
                    dest: './dist/js',
                    ext: '.js'
                }]
            }
        },
        injector: {
            options: {
                template: 'dist/index.html',
                addRootSlash: false,
                relative: true,
                destFile: 'dist/index.html'
            },
            publish: {
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: ['content/css/*.min.css', 'js/*.min.js']
                }]
            },
            dev_vendor: {
                options: {
                    transform: function(filePath) {
                        filePath = filePath.replace('/dist/', '');
                        return '<script src="' + filePath + '"></script>';
                    },
                    starttag: '<!-- injector:vendor:{{ext}} -->',
                    endtag: '<!-- endinjector -->',
                },
                files: {
                    'dist/index.html': [
                        [
                            'dist/vendor/flux/Flux.js',
                            'dist/vendor/react/react.js',
                            'dist/vendor/react/react-dom.js'
                        ],
                        [
                            'dist/vendor/**/*.js',
                            'dist/vendor/**/*.css'
                        ]
                    ]
                }
            },
            dev: {
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: ['content/css/*.css', 'js/*.js', '!content/css/*.min.css', '!js/*.min.js']
                }]
            }
        },
        copy: {
            dev: {
                files: [{
                    expand: true,
                    src: ['src/app/content/images/**'],
                    dest: 'dist/content/img/',
                    flatten: true,
                    filter: 'isFile'
                }, {
                    expand: true,
                    src: ['src/app/content/fonts/**'],
                    dest: 'dist/content/fonts/',
                    flatten: true,
                    filter: 'isFile'
                }, {
                    expand: true,
                    cwd: 'src/app',
                    src: ['index.html'],
                    dest: 'dist/'
                }, ]
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            publishMinimized: {
                files: [{
                    expand: true,
                    cwd: 'dist/content/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'dist/content/css',
                    ext: '.min.css'
                }]
            }
        },
        uglify: {
            options: {
                mangle: true,
                compress: true,
                preserveComments: false,
                screwIE8: true
            },
            publishMinimized: {
                files: {
                    'dist/js/app.bundle.min.js': ['dist/js/*.js', '!dist/js/*.min.js']
                }
            }
        },
        wiredep: {
            options: {
                directory: './dist/vendor'
            },
            target: {
                src: ['./dist/index.html']
            }
        },
        bower: {
            install: {
                options: {
                    targetDir: './dist/vendor',
                    cleanTargetDir: false,
                    cleanBowerDir: false,
                    copy: true
                }
            }
        },
        bowerBundle: {
            target: {
                src: ['dist/index.html']
            }
        },
        clean: {
            publish: {
                options: {
                    'force': true
                },
                src: ['dist']
            }
        },
        express: {
            dev: {
                options: {
                    port: 8080,
                    bases: 'dist',
                    hostname: '0.0.0.0',
                    livereload: true
                }
            }
        },
        open: {
            dev: {
                path: 'http://localhost:<%= express.dev.options.port%>/index.html'
            }
        },
        watch: {
            options: {
                livereload: true,
                spawn: false
            },
            css: {
                files: 'src/app/content/styles/less/**/**.less',
                tasks: ['newer:less']
            },
            jsx: {
                files: 'src/app/jsx/**/**.jsx',
                tasks: ['newer:browserify']
            }
        },
        concurrent: {
            dev_compile: ['less', 'browserify'],
            dev_inject: ['newer:wiredep', 'newer:injector:dev'],
            publish_compile: ['less', 'browserify'],
            publish_minify: ['cssmin:publishMinimized', 'uglify:publishMinimized'],
            publish_inject: ['bowerBundle', 'injector:publish']

        }
    });

    /* Define single tasks */
    grunt.registerTask('dev-open', ['concurrent:dev_compile', 'newer:copy', 'bower', 'injector:dev_vendor', 'injector:dev', 'express', 'open', 'watch']);
    grunt.registerTask('dev', ['concurrent:dev_compile', 'newer:copy', 'concurrent:dev_inject', 'express', 'watch']);
    grunt.registerTask('publish', ['concurrent:publish_compile', 'concurrent:publish_minify', 'copy', 'concurrent:publish_inject']);
};