var React = require('react');
var ReactDOM  = require('react-dom');

class App extends React.Component {
  constructor(name) {
    super();
    this.name = "Welcome home!";
  }

  render() {
    return (
        <h1>{this.name}</h1>
    );
  }
}

const app = document.getElementById('content');

ReactDOM.render(<App/>, app);
