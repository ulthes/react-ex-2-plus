import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import App from '../src/app/jsx/App';

describe('<App />', () => {

  it('renders name from constructor properly', () => {
    const wrapper = shallow(<App/>);
    expect(wrapper.text()).to.contain('Bla bla');
  });

});