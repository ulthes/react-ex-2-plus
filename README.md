# React Starter Toolkit EX 2 Plus [DEPRECATED]

This project is deprecated, use it on your own risk, many things don't work, you're free to use them however you want, but I will not update this repo in the future.

## Installation

### Clone repository
Go to your to the catalogue of your choice where you want to initialize toolkit and write in terminal/cmd:

```sh
$ git clone https://gitlab.com/ulthes/react-starter-toolkit.git
```
### Npm's package installation

After that run:

```sh
$ npm install
```

to install necessary dependencies located in `package.json`. 

## Catalogue Tree Structure

```
- dist/ <-- auto-generated project folder with ready-to-publish files,
- tst/  <-- folder containing tests and setup files,
- src/  <-- source files,
    - app/
        - jsx/ <-- source code for React components.
        - content/ <-- Images, Fonts, styles, all things that are not part of logic or structure of the page,
                - fonts/
                - images/
                - styles/
                        - base/
                        - main/
```

## Usage

Here are available commands:

### `$ npm run dev`

Runs tasks for compiling `.js`, `.less` files, copies those and all contents to `./dist` folder and then runs a lite-server and opens browser.
Connection stays open, so it will watch for any changes in files and then updates the browser, no need for reloading.

### `$ npm run tests`

Runs a Mocha test suite with Chai for every file in `./tst` folder. Shows test times afterwards in terminal/console.

### `$ npm run publish`

Compiles files, minifies them, puts everything from `/content` folder  to `./dist`, thus you can copy that folder to server for publishing.

## Enzyme Test API

Example test is located in `./tst` folder. The whole Enzyme syntax used for shallow rendering can be found [HERE](http://airbnb.io/enzyme/docs/api/shallow.html)